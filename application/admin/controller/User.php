<?php
/**
 * Created by PhpStorm.
 * User: jswei
 * Date: 2018/5/8
 * Time: 17:15
 */

namespace app\admin\controller;

use app\admin\validate\Admin as validateAdmin;

class User extends Base {
    public function __construct(){
        parent::__construct();
    }

    /**
     * @route('admin/login','post')
     *  ->allowCrossDomain()
     */
    public function index($pass=''){
        $admin = new validateAdmin;
        $data = request()->post();
        $data['password']=$pass;
        unset($data['pass']);
        if(!$admin->scene('login')->check($data)){
            return [
                'status'=>0,
                'msg'=>$admin->getError()
            ];
        }
        $admin = new \app\admin\model\Admin;
        $_admin = $admin::where('username','eq',$data['username'])->find();

        $flag = $this->_password($data['password'],1)==$_admin['password'];
        if(!$flag){
           return [
               'status'=>0,
               'msg'=>'密码错误'
           ];
        }
        unset($_admin['password']);
        unset($_admin['hash']);
        unset($_admin['update_time']);
        $address =  get_client_ip_address();
        $address = "{$address['country']},{$address['province']},{$address['city']}";
        $_admin->last_address=$address;
        $_admin->save();
        return [
            'status'=>1,
            'msg'=>"登录成功",
            'result'=>$_admin
        ];
    }

    /**
     * @route('admin/save','post')
     *  ->allowCrossDomain()
     */
    public function save(){
        $admin = new validateAdmin;
        $data = request()->post();
        if(!$admin->scene('save')->check($data)){
            return [
                'status'=>0,
                'msg'=>$admin->getError()
            ];
        }
        $admin = db('admin')
            ->field('id')
            ->where('username','eq',$data['username'])
            ->find();
        if(!$admin){
            return [
                'status'=>0,
                'msg'=>'管理员不存在'
            ];
        }
        $pwd = $this->_password($data['password'],1);
        if(!db('admin')->update([
            'id'=>$admin['id'],
            'password'=>$pwd,
            'dates'=>time()
        ])){
            return [
                'status'=>0,
                'msg'=>'修改失败'
            ];
        }
        return [
            'status'=>1,
            'msg'=>'修改成功'
        ];
    }
}