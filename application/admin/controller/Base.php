<?php
/**
 * Created by PhpStorm.
 * User: jswei
 * Date: 2018/5/8
 * Time: 17:16
 */
namespace app\admin\controller;
use think\Controller;

class Base extends Controller{
   public function __construct()
   {
       parent::__construct();
       header("Content-type:application/json");
       // 指定允许其他域名访问
       header('Access-Control-Allow-Origin:*');
       // 响应类型
       header('Access-Control-Allow-Methods:GET,POST,PUT,DELETE');
       // 响应头设置
       header('Access-Control-Allow-Headers:x-requested-with,content-type');
   }

    /**
     * 重置密码
     * @param $password
     * @param bool $flag
     * @return bool|string
     */
   protected function _password($password,$flag=false){
       return $flag?substr($password,10,15):substr(md5($password),10,15);
   }

    /**
     * 获取条件
     * @param array $_where
     * @return array
     */
   protected function _where($_where=[]){
       $map=[];
       foreach ($_where as $k => $v){
           $v = json_decode($v,true);
           if(isset($v['value'])){
               if(empty($v['value'])){
                   continue;
               }
               if(trim($v['op'])=='between'){
                   $start = $v['value'][0];
                   $end = $v['value'][1];
                   $map[]=[$v['field'],'between time',[$start,$end]];
               }else if(trim($v['op'])=='like'){
                   $val = trim($v['value']);
                   $map[] = [$v['field'],'like',"%{$val}%"];
               }else{
                   $val = trim($v['value']);
                   $map[] = [$v['field'],'=',$val];
               }
           }
       }
       return $map;
   }
}