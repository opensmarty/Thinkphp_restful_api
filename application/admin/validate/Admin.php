<?php
/**
 * Created by PhpStorm.
 * User: jswei
 * Date: 2018/4/24
 * Time: 9:13
 */

namespace app\admin\validate;
use think\Validate;

class Admin extends Validate
{
    protected $rule = [
        'username'  => 'require',
        'gid' => 'require'
    ];

    protected $message  =   [
        'username.require'      => '用户名必须',
        'gid.require'      => '密码必须'
    ];

    public function sceneLogin()
    {
        return $this->only(['username','password'])
            ->append('username', 'require')
            ->append('password', 'require');
    }
}