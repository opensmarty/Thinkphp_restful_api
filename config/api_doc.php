<?php
/**
 * Created by PhpStorm.
 * User: jswei
 * Date: 2018/4/20
 * Time: 11:09
 */

return [
    '1' => [
        'name' => '用户相关',
        'id' => '1',
        'parent' => '0',
        'class' => '',
        'readme' => ''
    ],//下面有子列表为一级目录
    '2' => [
        'name' => '接口说明',
        'id' => '2',
        'parent' => '1',
        'class' => '',
        'readme' => '/doc/md/auth.md'
    ],
    '3' => [
        'name' => '用户接口',
        'id' => '3',
        'parent' => '1',
        'readme' => '',
        'class' => app\first\controller\User::class
    ],
    '4' => [
        'name' => '栏目相关',
        'id' => '4',
        'parent' => '0',
        'class' => app\first\controller\Navbar::class
    ],
    '5' => [
        'name' => '轮播图相关',
        'id' => '5',
        'parent' => '0',
        'class' => app\first\controller\Carousel::class
    ],
];